<?php

function uc_geostore_schema(){
  $schema['uc_geostores'] = array(
    'description' => t('The base table for the geostores.'),
    'fields' => array(
      'stid' => array(
        'description' => t('The primary identifier for a geostore.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'name' => array(
        'description' => t('The geostore name.'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,),
      'currency' => array(
        'description' => t('The geostore currency.'),
        'type' => 'varchar',
        'length' => 3,
        'not null' => TRUE,),
      ),
    'primary key' => array('stid'),
    'unique keys' => array(
      'name' => array('name'),
    ),
  );

  $schema['uc_geostore_countries'] = array(
    'description' => t('The base table for the geostore countries.'),
    'fields' => array(
      'stid' => array(
        'description' => t('The primary identifier for a geostore.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'country' => array(
        'description' => t('The country ISO code.'),
        'type' => 'varchar',
        'length' => '3',
        'not null' => TRUE,),
      ),
    'foreign keys' => array(
      'uc_geostores' => array(
        'table' => 'uc_geostores',
        'columns' => array('stid', 'stid'),
        ),
      'countries_country' => array(
        'table' => 'countries_country',
        'columns' => array('country','iso2'),
        ),
      ),
    'unique keys' => array(
      'single_country' => array('country'),
    ),
  );

  $schema['uc_geostore_prices'] = array(
    'description' => t('The base table for the geostores pricing.'),
    'fields' => array(
      'stid' => array(
        'description' => t('The primary identifier for a geostore.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'type' => array(
        'description' => t('The content type to be priced differently.'),
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,),
      'price' => array(
        'description' => t('The price for this content type.'),
        'type' => 'float',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      ),
    'unique keys' => array(
      'single_price' => array('stid', 'type'),
    ),
  );

  $schema['uc_geostore_product_prices'] = array(
    'description' => t('The base table for the geostores pricing.'),
    'fields' => array(
      'pid' => array(
        'description' => t('The primary identifier for the geostore product price.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'pfid' => array(
        'description' => t('The primary identifier of the product feature.'),
        'type' => 'int',
        'unsigned' => TRUE,),
      'stid' => array(
        'description' => t('The primary identifier for a geostore.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'nid' => array(
        'description' => t('The primary identifier of a product node.'),
        'type' => 'int',
        'not null' => TRUE,),
      'price' => array(
        'description' => t('The price for this product store.'),
        'type' => 'float',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      ),
    'primary key' => array('pid'),
    'unique keys' => array(
      'single_price' => array('stid', 'nid'),
    ),
  );

  return $schema;
}