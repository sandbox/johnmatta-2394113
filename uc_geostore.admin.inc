<?php

function uc_geostore_settings_form($form = array()){
  $types = node_type_get_types();
  $defaults = variable_get('uc_geostore_product_types', array() );
  $options = array();

  foreach($types as $type){
    $options[$type->name] = $type->name;
  }

  $form['types'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Which content types should special pricing be applied to?',
    '#options' => $options,
    '#default_value' => $defaults,
    );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',);

  return $form;
}

function uc_geostore_settings_form_submit($form, &$form_state){
  $values = $form_state['values'];
  $types = array();
  foreach($values['types'] as $name => $chosen){
    if(gettype($chosen) == 'string'){
      $types[] = $chosen;
    }
  }

  variable_set('uc_geostore_product_types', $types);
  drupal_set_message(t('Product types saved successfully.'));
  drupal_goto('/admin/store/geostores');
}

function uc_geostore_list_form($form = array()){
  $header = array('Store', 'Operations');

  $stores = db_select('uc_geostores', 'g')->fields('g')->execute();

  $rows = array();
  foreach($stores as $store){
    $rows[] = array($store->name, l('edit', 'admin/store/geostores/edit/'.$store->stid).'&nbsp&nbsp'.l('delete', 'admin/store/geostores/delete/'.$store->stid));
  }

  $form['add_store'] = array(
    '#type' => 'link',
    '#title' => t('Add a new store'),
    '#href' => 'admin/store/geostores/add',
    );
  if(count($rows) == 0){
    $form['stores'] = array(
      '#markup' => '<p>'.t("There are no geostores yet!"),
      );
  }else{
    $form['stores'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      );
  }
  return $form;
}

function uc_geostore_add_store_form($form = array()){
  $types = variable_get('uc_geostore_product_types', array() );
  if(count($types) == 0){
    drupal_set_message(t('You need to choose your product content types before adding stores'), 'error');
    drupal_goto('admin/store/settings/geostores');
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => 'Store name',
    '#required' => TRUE,
    );

  $form['currency'] = array(
    '#type' => 'textfield',
    '#title' => 'Store currency',
    '#size' => 3,
    '#max_length' => 3,
    '#required' => TRUE,
    );

  $countries =  countries_get_countries( 'name', array('enabled' => COUNTRIES_ENABLED) );

  $form['countries'] = array(
    '#type' => 'fieldset',
    '#title' => 'Countries',
    '#collapsible' => TRUE,
    );
  $form['countries']['checkboxes'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Countries that direct to that store',
    '#options' => $countries,
    );

  $form['prices'] = array(
    '#type' => 'fieldset',
    '#title' => 'Prices',
    '#collapsible' => TRUE,
    );

  foreach($types as $type){
    $form['prices'][$type] = array(
      '#type' => 'textfield',
      '#title' => $type,
      '#required' => TRUE,
      );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',);

  return $form;
}

function uc_geostore_add_store_form_submit($form, &$form_state){
  $values = $form_state['values'];
  $transaction = db_transaction();

  $name = $values['name'];
  $stid = db_insert('uc_geostores')->fields(array('name'=>$name, 'currency'=> $values['currency']))->execute();

  foreach($values['checkboxes'] as $name => $chosen){
    if(gettype($chosen) == 'string'){
      db_insert('uc_geostore_countries')->fields(array('stid' => $stid, 'country' => $chosen))->execute();
    }
  }

  $types = variable_get('uc_geostore_product_types', array() );
  foreach ($types as $type) {
    db_insert('uc_geostore_prices')->fields(array('stid' => $stid, 'type' => $type, 'price' => $values[$type]))->execute();
  }

  drupal_goto('admin/store/geostores');
}

function uc_geostore_edit_store_form($form = array(), &$form_state, $stid){
  $form = uc_geostore_add_store_form($form);
  $form['stid'] = array(
    '#type' => 'value',
    '#value' => $stid,
    );
  $name = db_select('uc_geostores', 'g')->fields('g')->condition('stid', $stid)->execute()->fetch();
  if(!$name){
    drupal_set_message(t('This store ID doesn\'t exist'), 'error');
    drupal_goto('admin/store/geostores');
  }
  $form['name']['#default_value'] = $name->name;
  
  $countries = db_select('uc_geostore_countries', 'g')->fields('g')->condition('stid', $stid)->execute();
  $defaults = array();
  foreach ($countries as $country) {
    $defaults[] = $country->country;
  }
  $form['countries']['checkboxes']['#default_value'] = $defaults;

  $prices = db_select('uc_geostore_prices', 'g')->fields('g')->condition('stid', $stid)->execute();
  foreach ($prices as $price) {
    $form['prices'][$price->type]['#default_value'] = $price->price;
  }

  return $form;
}

function uc_geostore_edit_store_form_submit($form, &$form_state){
  $values = $form_state['values'];
  $stid = $values['stid'];
  $transaction = db_transaction();
  db_update('uc_geostores')->condition('stid', $stid)->fields(array('name'=> $values['name']))->execute();

  db_delete('uc_geostore_countries')->condition('stid', $stid)->execute();
  db_delete('uc_geostore_prices')->condition('stid', $stid)->execute();

  foreach($values['checkboxes'] as $name => $chosen){
    if(gettype($chosen) == 'string'){
      db_insert('uc_geostore_countries')->fields(array('stid' => $stid, 'country' => $chosen))->execute();
    }
  }

  $types = variable_get('uc_geostore_product_types', array() );
  foreach ($types as $type) {
    db_insert('uc_geostore_prices')->fields(array('stid' => $stid, 'type' => $type, 'price' => $values[$type]))->execute();
  }

  drupal_goto('admin/store/geostores');
}

function uc_geostore_delete_store($stid){
  $name = db_select('uc_geostores', 'g')->fields('g')->condition('stid', $stid)->execute()->fetch();
  if(!$name){
    drupal_set_message(t('This store ID doesn\'t exist'), 'error');
    drupal_goto('admin/store/geostores');
  }
  db_delete('uc_geostore_countries')->condition('stid', $stid)->execute();
  db_delete('uc_geostore_prices')->condition('stid', $stid)->execute();
  db_delete('uc_geostores')->condition('stid', $stid)->execute();
  drupal_goto('admin/store/geostores');
}